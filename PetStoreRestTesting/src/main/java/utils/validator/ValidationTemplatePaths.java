package utils.validator;

public class ValidationTemplatePaths {
    public static final String PATH_TO_PET_TEMPLATE = "src/main/resources/pet_models/Pet.json";
    public static final String PATH_TO_PETLIST_TEMPLATE = "src/main/resources/pet_models/PetList.json";
}