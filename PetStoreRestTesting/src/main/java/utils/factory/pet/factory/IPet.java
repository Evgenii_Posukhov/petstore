package utils.factory.pet.factory;

import swagger.petstore.models.PetModel;

public interface IPet {
    PetModel createPet();
}