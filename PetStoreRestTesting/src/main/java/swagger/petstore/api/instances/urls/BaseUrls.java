package swagger.petstore.api.instances.urls;

public class BaseUrls {
    public static final String BASE_URL = "https://petstore.swagger.io/v2";
}